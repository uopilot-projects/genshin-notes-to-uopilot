# Genshin notes to UOpilot

==================================

Скрипт переводит ноты мелодий, например, такого типа

    SC----D----F----HMD----J---------JD----QM
    WC---------Q----NA----J---------HD----M
    HC--------------MD----J---------JD----QM
    WC--------W-Q----NA---Q-J----A----J-HM----X
    SC----D----F----HMD----J---------JC----QX
    WV---------Q----AF----J---------HX
    JC--------------JMD-Q-W-E---------MC----AV
    SC---------A----CM----J----C--Q--S----WM
    EV----R----E----HAF----E---------EV----C--R-E
    WV-------E-W-Q----AF----Q---------JB
    HN--------------NA--H-J-Q---------C--C--V--C
    N--------------NA----J-------Q--C----W
    EVA----R----E----HAD----E---------EVA-------R-E
    WMH-------E-W-Q---------QMD--------------J
    N---------H----AD--------------C
    N--------------DH--------------C--C--V--C
    N

В формат для UOpilot

    set SendExDelay 0
    sendex SC
    set SendExDelay 40
    sendex ----
    set SendExDelay 0
    sendex D
    ...

Перевод происходит "на лету". Копируете исходный текст, например, в блокнот, ставите курсор на начало и запускаете скрипт. Необходимо убедиться, что включена английская расскладка клавиатуры. Пауза между строками в миллисекундах в настройках UOpilot устанавливается в значение 0.

Ниже представлен код скрипта

    // Genshin notes to UOpilot
    set $delay 75  // задержка между нажатиями
    set #str_delay 150  // задержка новой строки
    set $symbol_pause -  // символ паузы

    set $var setlayout (0409)
    set $last 0
    set $a 1
    sendex {Enter}
    sendex ~{Left}
    sendex ^{X}
    get clipboard $enter

    while 0 = 0
        sendex ~{Right}
        sendex ^{X}
        get clipboard $a

            if $a = $enter
                sendex {Enter}
                sendex wait #str_delay
            end_if

            if $a != $symbol_pause and ($last = $symbol_pause or $last = 0 or $last = $enter)
                if $last != 0
                    sendex {Enter}
                end_if
                sendex "set SendExDelay 0"
                sendex {Enter}
                sendex sendex
                sendex {Space}
                if $a != $enter
                    sendex ^{V}
                end_if
            end_if
            
            if $a = $symbol_pause and $last != $symbol_pause
                sendex {Enter}
                sendex "set SendExDelay "
                sendex $delay
                sendex {Enter}
                sendex sendex
                sendex {Space}
                sendex ^{V}
            end_if

            if ($a != $enter) and (($a = $symbol_pause and $last = $symbol_pause) or ($a != $symbol_pause and $last != $symbol_pause and $last != 0 and $last != $enter))
                sendex ^{V}
            end_if

            if $last = $a and $a != $symbol_pause
                sendex {Backspace}
                sendex {Enter}
                sendex end_script
                end_script
            end_if

            set $last $a

    end_while
    end_script
